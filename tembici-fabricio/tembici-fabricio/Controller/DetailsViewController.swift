//
//  DetailsViewController.swift
//  tembici-fabricio
//
//  Created by Fabricio Rodrigues on 20/06/2018.
//  Copyright © 2018 Fabricio Rodrigues. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {
    
    @IBOutlet weak var imageMovie:UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelYear: UILabel!
    @IBOutlet weak var labelGenre: UILabel!
    @IBOutlet weak var textViewOverview:UITextView!
    @IBOutlet weak var btFavorite: UIButton!
    
    let item = DBManager.sharedInstance.getDataFromDB()
    
    var objReceived:Movie!
    var objFavorites: MovieObject!
    var arrayFavorites: [Movie] = []
    var textGenre = ""
    var objDetail: MovieObject!
    
    private var listGenres = Array<Genre>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if objReceived == nil {
            objDetail = objFavorites
        } else {
            objDetail = objReceived.movieObject
        }
        
        let urlImage: String = String.init(format: "http://image.tmdb.org/t/p/w185/%@",objDetail.imageName!)
        imageMovie.sd_setImage(with: URL(string: urlImage), placeholderImage: UIImage(named: ""))
        
        labelTitle.text = objDetail.title
        let index = objDetail.release_date?.index(of: "-")!
        let newString = String((objDetail.release_date?.prefix(upTo: index!))!)
        labelYear.text = newString
        
        textViewOverview.text = objDetail.overview
        
        
        btFavorite.setImage(UIImage.init(named: "image_fav_empty"), for: .normal)
        btFavorite.setImage(UIImage.init(named: "image_fav_full"), for: .disabled)
        
        if item.contains(where: { $0.title == objDetail.title }) {
            btFavorite.isEnabled = false
        } else {
            
            btFavorite.isEnabled = true
        }
        
        self.requestGenre(page: 1)
        
    }
    
    func requestGenre(page: Int) -> Void {
        
        GenresRequest.getListGenres(page: page) { (genres, error) in
            
            if let newListGenres = genres {
                
                self.listGenres.append(contentsOf: newListGenres)
                
                
                print(self.listGenres.count)
                
                
                
                for i in 0..<self.objDetail.arrayGenres.count {
                    
                    var arrayCopy = self.listGenres
                    
                    if arrayCopy.contains(where: {$0.genreObject.idGenre == self.objDetail.arrayGenres[i]}) {
                        arrayCopy = arrayCopy.filter{
                            
                            ($0.genreObject.idGenre == self.objDetail.arrayGenres[i])
                            
                            
                        }
                        
                        let newObj = arrayCopy[0]
                        
                        self.textGenre.append(newObj.genreObject.titleGenre!+",")
                        
                    } else {
                        
                    }
                }
                
                self.labelGenre.text = self.textGenre
                
            } else if(genres == nil) {
                //                self.alertDados()
            }
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.view = nil
    }
    
    //MARK: Actions
    @IBAction func saveFavorites(){
        
        if(objReceived != nil) {
            objReceived.movieObject.ID = DBManager.sharedInstance.getDataFromDB().count
        }
        
        DBManager.sharedInstance.addData(object: objReceived.movieObject)
        
        btFavorite.isEnabled = false

        self.dismiss(animated: true) { }
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}


