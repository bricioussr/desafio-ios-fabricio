//
//  FavoritesViewController.swift
//  tembici-fabricio
//
//  Created by Fabricio Rodrigues on 20/06/2018.
//  Copyright © 2018 Fabricio Rodrigues. All rights reserved.
//

import UIKit
import RealmSwift

class FavoritesViewController: UIViewController {
    
    @IBOutlet weak var mySearchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btFilter: UIButton!
    @IBOutlet var labelSearch: UILabel!
    @IBOutlet var viewNotFound: UIView!
    
    var filteredMovies: [MovieObject]!
    var arrayMovies = [MovieObject]()
    var objSelected: MovieObject!
    
    var item = DBManager.sharedInstance.getDataFromDB()


    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewNotFound.isHidden = true
        
        
        for i in 0..<item.count {
            arrayMovies.append(item[i])
        }
        
        filteredMovies = arrayMovies
        
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.reloadData()
        
        print("List \(item)")

        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "segueDetails"){
            var vc:DetailsViewController = DetailsViewController()
            vc = segue.destination as! DetailsViewController
            vc.objFavorites = objSelected
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(true)
        
        self.mySearchBar.delegate = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        tabBarController?.tabBar.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        arrayMovies.removeAll()
        filteredMovies.removeAll()
        self.view = nil
    }
    
    
    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
        
    }
    
    //Mark: Actions
    @IBAction func removeFilter(){
        
        searchBar(mySearchBar, textDidChange: "")
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension FavoritesViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if filteredMovies != nil {
            return filteredMovies.count
        } else {
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:FavoritesTableViewCell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! FavoritesTableViewCell
        
        let objMovie = filteredMovies[indexPath.row]
        
        cell.labelTitle.text = objMovie.title
        let index = objMovie.release_date?.index(of: "-")!
        let newString = String((objMovie.release_date?.prefix(upTo: index!))!)
        cell.labelYear.text = newString
        cell.textDescription.text = objMovie.overview
        
        let urlImage: String = String.init(format: "http://image.tmdb.org/t/p/w185/%@",objMovie.imageName!)
        cell.imageMovie.sd_setImage(with: URL(string: urlImage), placeholderImage: UIImage(named: ""))
        
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120.0
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "Unfavorite"
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            
            
            print("Before delete \(item.count)")

            DBManager.sharedInstance.deleteFromDb(object: item[indexPath.row])
            
            print("After Delete \(item.count)")
            
            arrayMovies.remove(at: indexPath.row)
            
            filteredMovies = arrayMovies
            
            print("List \(item)")
            
            
            tableView.beginUpdates()
            tableView.deleteRows(at: [indexPath], with: .middle)
            tableView.endUpdates()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        objSelected = filteredMovies[indexPath.row]
        self.performSegue(withIdentifier: "segueDetails", sender: self)
    }
    
}

extension FavoritesViewController: UISearchBarDelegate{
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText == "" {
            filteredMovies = arrayMovies
            
            viewNotFound.isHidden = true

        } else {
             item = DBManager.sharedInstance.getDataFromDB()

            self.filteredMovies = item.filter {
                ($0.title?.lowercased().contains(searchBar.text!.lowercased()))!
            }
            
            if filteredMovies.count == 0 {
                labelSearch.text = String.init(format: "Sua busca por \" %@ \" não resultou em nenhum resultado", mySearchBar.text!)
                //
                viewNotFound.isHidden = false
            }
        }
        
        tableView.reloadData()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        mySearchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        mySearchBar.resignFirstResponder()
    }
}
