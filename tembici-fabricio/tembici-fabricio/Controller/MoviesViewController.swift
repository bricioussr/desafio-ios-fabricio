//
//  MoviesViewController.swift
//  tembici-fabricio
//
//  Created by Fabricio Rodrigues on 20/06/2018.
//  Copyright © 2018 Fabricio Rodrigues. All rights reserved.
//

import UIKit
import SDWebImage
import RealmSwift

class MoviesViewController: UIViewController {
    
    @IBOutlet weak var mySearchBar: UISearchBar!
    @IBOutlet weak var labelSearch: UILabel!
    @IBOutlet weak var viewNotFound: UIView!
    @IBOutlet weak var imageNotFoundView: UIImageView!
    @IBOutlet weak var collectionView:UICollectionView!
    
    var receiveCategory:String!
    var pageMovie:Int!
    private var listMovies = Array<Movie>()
    var objSelected:Movie!
    var filteredMovies = Array<Movie>()
    var moviesResults:Array<Movie>!
    var notificationToken: NotificationToken? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewNotFound.isHidden = true
        
        pageMovie = 1
        
        self.checkDataMovies()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        collectionView.reloadData()

        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        //

        self.mySearchBar.delegate = self
        self.mySearchBar.text = ""
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)

        tabBarController?.tabBar.isHidden = false
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        self.listMovies.removeAll()
        self.filteredMovies.removeAll()
        self.view = nil
    }
    
    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueDetails" {
            var vc:DetailsViewController = DetailsViewController()
            vc = segue.destination as! DetailsViewController
            vc.objReceived = objSelected
        }
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension MoviesViewController {
    
    func requestMovie(page: Int) -> Void {
        
        MoviesRequest.getListMovies(page: page) { (movies, error) in
            
            if let newListMovies = movies {
                
                self.listMovies.append(contentsOf: newListMovies)
                
                self.filteredMovies = self.listMovies
                
                self.collectionView.reloadData()
                
            } else if(movies == nil) {
                self.imageNotFoundView.image = UIImage.init(named: "image_error")
                self.viewNotFound.isHidden = false
                self.labelSearch.text = "Um erro ocorreu. Por favor, tente novamente."
            }
        }
        
    }
    
    func checkDataMovies(){
            requestMovie(page: pageMovie)
    }
}

extension MoviesViewController:  UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filteredMovies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:MoviesCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "movieCell", for: indexPath) as! MoviesCollectionViewCell
        
        let obj:Movie = self.filteredMovies[indexPath.row]
        
        let item = DBManager.sharedInstance.getDataFromDB()

            if  item.contains(where: { $0.title == obj.movieObject.title }) && item.isInvalidated != true {
                cell.imageViewFav.image = UIImage.init(named: "image_fav_full")

            } else {
                cell.imageViewFav.image = UIImage.init(named: "image_fav_empty")
                
            }
        
        cell.labelTitle.text = obj.movieObject.title
        //
        let urlImage: String = String.init(format: "http://image.tmdb.org/t/p/w185/%@",obj.movieObject.imageName!)
        cell.imageViewMovie.sd_setImage(with: URL(string: urlImage), placeholderImage: UIImage(named: "icon_movie"))
        
        return cell
        
    }
    
    // Permite realizar a paginação dos filmes
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if (indexPath.row == listMovies.count - 1) {
            pageMovie = pageMovie + 1
            self.requestMovie(page: pageMovie)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        objSelected = self.filteredMovies[indexPath.row]
        
        if objSelected != nil {
            self.performSegue(withIdentifier: "segueDetails", sender: self)
        }
        
    }
}

extension MoviesViewController: UISearchBarDelegate{
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            
            viewNotFound.isHidden = true
            
            filteredMovies = listMovies
        } else {
            
            self.filteredMovies = listMovies.filter {
                ($0.movieObject.title?.lowercased().contains(searchBar.text!.lowercased()))!
            }
            
            if self.filteredMovies.count == 0 {
                labelSearch.text = String.init(format: "Sua busca por \" %@ \" não resultou em nenhum resultado", mySearchBar.text!)
                //
                self.imageNotFoundView.image = UIImage.init(named: "image_search")
                viewNotFound.isHidden = false
                labelSearch.isHidden = false
            }
        }
        collectionView.reloadData()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        mySearchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        mySearchBar.resignFirstResponder()
    }
}


