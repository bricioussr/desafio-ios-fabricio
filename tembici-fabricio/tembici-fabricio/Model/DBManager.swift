//
//  DBManager.swift
//  tembici-fabricio
//
//  Created by Fabricio Rodrigues on 20/06/2018.
//  Copyright © 2018 Fabricio Rodrigues. All rights reserved.
//

import UIKit
import RealmSwift

class DBManager {
    
    private var database:Realm
    static let sharedInstance = DBManager()
    
    private init() {
        
        database = try! Realm()
        database.autorefresh = true
        
    }
    
    func getDataFromDB() -> Results<MovieObject> {
        
        let results: Results<MovieObject> = database.objects(MovieObject.self)
        return results
    }
    
    func addData(object: MovieObject) {
        
        try! database.write {
            database.add(object, update: true)
        }
    }
    
    func deleteAllDatabase()  {
        try! database.write {
            database.deleteAll()
        }
    }
    
    func deleteFromDb(object: MovieObject) {
        
        try! database.write {
            
            database.delete(object)
        
            
        }
        
    }
    
}

