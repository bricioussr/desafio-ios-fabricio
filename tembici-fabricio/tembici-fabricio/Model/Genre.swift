//
//  Genre.swift
//  tembici-fabricio
//
//  Created by Fabricio Rodrigues on 20/06/2018.
//  Copyright © 2018 Fabricio Rodrigues. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift
import ObjectMapperAdditions
import ObjectMapper_Realm

struct Genres: Mappable {
    var genres: [Genre]
    
    init?(map: Map) {
        do {
            genres = try map.value("genres")
        } catch {
            return nil
        }
    }
    
    mutating func mapping(map: Map) {
        genres <- map["genres"]
    }
}

struct Genre: Mappable {
    
    let genreObject = GenreObject()
    
    init?(map: Map) {
        
    }
    
    // Mappable
    mutating func mapping(map: Map) {
        
        genreObject.idGenre            <- map["id"]
        genreObject.titleGenre         <- map["name"]
        
    }
}

