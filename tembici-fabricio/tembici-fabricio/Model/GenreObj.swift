//
//  GenreObj.swift
//  tembici-fabricio
//
//  Created by Fabricio Rodrigues on 20/06/2018.
//  Copyright © 2018 Fabricio Rodrigues. All rights reserved.
//

import UIKit

class GenreObject: NSObject {
    
    var idGenre: Int?
    var titleGenre: String?
    
}
