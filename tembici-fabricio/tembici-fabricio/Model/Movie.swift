//
//  Movie.swift
//  tembici-fabricio
//
//  Created by Fabricio Rodrigues on 20/06/2018.
//  Copyright © 2018 Fabricio Rodrigues. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift
import ObjectMapperAdditions
import ObjectMapper_Realm

struct Movies: Mappable {
    var movies: [Movie]
    
    init?(map: Map) {
        do {
            movies = try map.value("results")
        } catch {
            return nil
        }
    }
    
    mutating func mapping(map: Map) {
        movies <- map["results"]
    }
}

struct Movie: Mappable {
    
    let movieObject = MovieObject()
    
    init?(map: Map) {
        
    }
    
    // Mappable
    mutating func mapping(map: Map) {
        
        movieObject.title           <- map["title"]
        movieObject.poster_path     <- map["poster_path"]
        movieObject.array           <- map["results"]
        movieObject.release_date    <- map["release_date"]
        movieObject.overview        <- map["overview"]
        movieObject.imageName       <- map["poster_path"]
        movieObject.arrayGenres <- (map["genre_ids"], RealmTypeCastTransform())
        
    }
    
    
}
