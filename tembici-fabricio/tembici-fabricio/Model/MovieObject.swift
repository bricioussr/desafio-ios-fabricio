//
//  MovieObject.swift
//  tembici-fabricio
//
//  Created by Fabricio Rodrigues on 20/06/2018.
//  Copyright © 2018 Fabricio Rodrigues. All rights reserved.
//

import UIKit
import RealmSwift
import Realm

class MovieObject: Object  {
    
    @objc dynamic var title: String?
    @objc dynamic var poster_path: String?
    var array: [Any]?
    @objc dynamic var release_date: String?
    @objc dynamic var overview: String?
    @objc dynamic var imageName: String?
    var arrayGenres: List<Int> = List<Int>()
    
    @objc dynamic var ID = -1
    
    override static func primaryKey() -> String? {
        return "ID"
    }
    
}
