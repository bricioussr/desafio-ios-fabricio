//
//  GenresRequest.swift
//  tembici-fabricio
//
//  Created by Fabricio Rodrigues on 20/06/2018.
//  Copyright © 2018 Fabricio Rodrigues. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import PKHUD


class GenresRequest: NSObject {
    
    var projects:[Genres] = []
    
    class func getListGenres(page: Int, completion: @escaping ([Genre]?, Error?) -> Void) {
        
        let urlString:String = String.init(format: "https://api.themoviedb.org/3/genre/movie/list?api_key=c8676c30c72e30216028908b86a31b13&language=en-US")
        
        PKHUD.sharedHUD.contentView = PKHUDProgressView()
        PKHUD.sharedHUD.show()
        
        Alamofire.request(urlString, parameters: nil)
            .validate(contentType: ["application/json"]).responseObject { (response: DataResponse<Genres>) in
                switch(response.result){
                    
                case Result.success:
                    if let response = response.result.value {
                        completion(response.genres, nil)
                    }
                    break
                    
                default:
                    completion(nil, response.error)
                    break
                }
                
                PKHUD.sharedHUD.hide()
        }
    }
    
}
