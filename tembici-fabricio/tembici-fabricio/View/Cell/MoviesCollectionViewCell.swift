//
//  MoviesCollectionViewCell.swift
//  tembici-fabricio
//
//  Created by Fabricio Rodrigues on 20/06/2018.
//  Copyright © 2018 Fabricio Rodrigues. All rights reserved.
//

import UIKit

class MoviesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageViewMovie: UIImageView!
    @IBOutlet weak var imageViewFav: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    
}
