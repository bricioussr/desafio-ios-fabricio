//
//  tembici_fabricioTests.swift
//  tembici-fabricioTests
//
//  Created by Fabricio Rodrigues on 20/06/2018.
//  Copyright © 2018 Fabricio Rodrigues. All rights reserved.
//

import XCTest
@testable import tembici_fabricio

class tembici_fabricioTests: XCTestCase {
    
    var moviesVC = MoviesViewController()
    var detailsVC = DetailsViewController()
    var favoritesVC = FavoritesViewController()
    
    
    override func setUp() {
        super.setUp()
        
        let storyboard = UIStoryboard(name: "Main",bundle: nil)
        
        moviesVC = storyboard.instantiateViewController(withIdentifier: "MoviesViewController") as! MoviesViewController
        moviesVC.loadViewIfNeeded()
        
        favoritesVC = storyboard.instantiateViewController(withIdentifier: "FavoritesViewController") as! FavoritesViewController
        favoritesVC.loadViewIfNeeded()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testCollection() {
        XCTAssertNotNil(moviesVC.collectionView)
    }
    
    func testRequest() {
        
        MoviesRequest.getListMovies(page: 1) { (movies, error) in
            
            if let newListMovies = movies {
                
                XCTAssertNotNil(newListMovies)
            }
        }
    }
    
    func testRequestGenre() {
        GenresRequest.getListGenres(page: 1) { (genres, error) in
            
            if let newListGenres = genres {
                XCTAssertNotNil(newListGenres)
            }
            
        }
    }
    
    
    
}
