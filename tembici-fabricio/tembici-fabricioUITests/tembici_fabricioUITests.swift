//
//  tembici_fabricioUITests.swift
//  tembici-fabricioUITests
//
//  Created by Fabricio Rodrigues on 20/06/2018.
//  Copyright © 2018 Fabricio Rodrigues. All rights reserved.
//

import XCTest

class tembici_fabricioUITests: XCTestCase {
    
    var app:XCUIApplication!
        
    override func setUp() {
        super.setUp()
        
        continueAfterFailure = false
        app = XCUIApplication()
        
        
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testCallDetailsViewController() {
        
        let collectionView = app.collectionViews["collectionView"]
        XCTAssertNotNil(collectionView.cells)
        let firstCell = app.collectionViews.children(matching: .any).element(boundBy: 0)
        
        if firstCell.exists {
            firstCell.tap()
        }
            
    }
    
    
}
